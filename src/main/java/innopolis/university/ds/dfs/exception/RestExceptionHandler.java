package innopolis.university.ds.dfs.exception;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Log4j2
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String DEFAULT_INTERNAL_SERVER_ERROR_MESSAGE = "Internal server error.";
    private static final String DEFAULT_VALIDATION_ERROR_MESSAGE = "Invalid method parameters.";

    @ExceptionHandler(Throwable.class)
    public @ResponseBody
    ResponseEntity handleException(Throwable t) {
        Throwable rootCause = t.getCause();
        Throwable rootException = rootCause != null ? rootCause : t;
        log.error(rootException.getMessage(), rootException);

        String exceptionMessage = rootException.getMessage();
        HttpStatus statusCode = defineStatusCodeByExceptionType(rootException);

        if (StringUtils.isEmpty(exceptionMessage)) {
            exceptionMessage = rootException.getClass().getSimpleName();
        }

        if (statusCode == HttpStatus.FORBIDDEN && rootException instanceof ForbiddenException) {
            return new ResponseEntity<>(exceptionMessage, statusCode);
        }

        if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR) {
            log.error("REST API request failed.", t);
            exceptionMessage = DEFAULT_INTERNAL_SERVER_ERROR_MESSAGE;
        }

        return new ResponseEntity<>(exceptionMessage, statusCode);
    }

    private HttpStatus defineStatusCodeByExceptionType(Throwable exception) {
        if (exception instanceof NotFoundException) {
            return HttpStatus.NOT_FOUND;
        }
        if (exception instanceof ForbiddenException) {
            return HttpStatus.FORBIDDEN;
        }
        if (exception instanceof BadRequestException) {
            return HttpStatus.BAD_REQUEST;
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        log.error(String.format("REST API validation failed, error: [%s]", ex.getMessage()));

        return new ResponseEntity<>(DEFAULT_VALIDATION_ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers, HttpStatus status,
                                                         WebRequest request) {
        log.error(String.format("REST API bind failed, error: [%s]", ex.getMessage()));

        return new ResponseEntity<>(DEFAULT_VALIDATION_ERROR_MESSAGE, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(
            HttpMessageNotReadableException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request
    ) {
        Throwable rootCause = ex.getRootCause();
        if (rootCause instanceof BadRequestException) {
            return new ResponseEntity<>(rootCause.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return super.handleHttpMessageNotReadable(ex, headers, status, request);
    }
}
