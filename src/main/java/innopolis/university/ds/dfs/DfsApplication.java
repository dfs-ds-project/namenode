package innopolis.university.ds.dfs;

import innopolis.university.ds.dfs.system.RequestHandlerInterceptor;
import innopolis.university.ds.dfs.system.System;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableScheduling
@SpringBootApplication
@RequiredArgsConstructor
public class DfsApplication implements WebMvcConfigurer {

    public static void main(String[] args) {
        SpringApplication.run(DfsApplication.class, args);
    }

    private final System system;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new RequestHandlerInterceptor(system));
    }
}
