package innopolis.university.ds.dfs.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class DataNodeHost {

    @JsonProperty("host")
    private String host;

    @JsonProperty("restored")
    private boolean restored;
}
