package innopolis.university.ds.dfs.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;

@Data
@AllArgsConstructor
public class Directories {

    @JsonProperty("dirs")
    private Collection<String> dirs;
}
