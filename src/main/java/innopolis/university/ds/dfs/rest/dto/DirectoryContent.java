package innopolis.university.ds.dfs.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Collection;

@Data
public class DirectoryContent {

    @JsonProperty("dirs")
    private Collection<String> dirs;

    @JsonProperty("files")
    private Collection<String> files;
}
