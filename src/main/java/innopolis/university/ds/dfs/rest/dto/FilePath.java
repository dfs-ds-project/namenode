package innopolis.university.ds.dfs.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class FilePath {

    @JsonProperty("path")
    private String path;
}