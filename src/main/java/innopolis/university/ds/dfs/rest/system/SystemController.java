package innopolis.university.ds.dfs.rest.system;

import innopolis.university.ds.dfs.rest.dto.DataNodeHost;
import innopolis.university.ds.dfs.system.System;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.logging.LogLevel;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.tinkoff.eclair.annotation.Log;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
@RestController
@RequiredArgsConstructor
public class SystemController {

    private final System system;

    @Log(LogLevel.INFO)
    @PostMapping(value = "/system/init", produces = {"application/json"})
    public ResponseEntity<Long> initializeSystem() {
        return ResponseEntity.ok(system.initialize());
    }

    @Log(LogLevel.INFO)
    @PostMapping(value = "/system/datanodes", produces = {"application/json"})
    public ResponseEntity<Void> addDataNode(
            @NotNull
            @Valid
            @RequestBody
                    DataNodeHost host
    ) {
        system.addDataNode(host.getHost(), host.isRestored());
        return ResponseEntity.ok().build();
    }
}
