package innopolis.university.ds.dfs.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class NewFilePath {

    @JsonProperty("newPath")
    private String newPath;
}
