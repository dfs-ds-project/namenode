package innopolis.university.ds.dfs.rest.filesystem;

import innopolis.university.ds.dfs.rest.dto.*;
import innopolis.university.ds.dfs.system.System;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.logging.LogLevel;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.tinkoff.eclair.annotation.Log;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Validated
@RestController
@RequiredArgsConstructor
public class FileSystemController {

    private final System system;

    @Log(LogLevel.INFO)
    @PutMapping(value = "/filesystem/files", consumes = {"application/json"})
    public ResponseEntity<Void> copyFile(
            @NotNull
            @Valid
            @RequestParam(value = "path")
                    String path,
            @NotNull
            @Valid
            @RequestBody
                    NewFilePath newFilePath) {
        system.lockDataNodes();
        system.copyFile(path, newFilePath.getNewPath());
        return ResponseEntity.ok().build();
    }

    @Log(LogLevel.INFO)
    @DeleteMapping(value = "/filesystem/files")
    public ResponseEntity<Void> deleteFile(
            @NotNull
            @Valid
            @RequestParam(value = "path")
                    String path) {
        system.lockDataNodes();
        system.deleteFile(path);
        return ResponseEntity.ok().build();
    }

    @Log(LogLevel.INFO)
    @GetMapping(value = "/filesystem/files/info", produces = {"application/json"})
    public ResponseEntity<FileInfo> getFileInfo(
            @NotNull
            @Valid
            @RequestParam(value = "path")
                    String path) {
        return ResponseEntity.ok(system.getFileInfo(path).toDto());
    }

    @Log(LogLevel.INFO)
    @PatchMapping(value = "/filesystem/files", consumes = {"application/json"})
    public ResponseEntity<Void> moveFile(
            @NotNull
            @Valid
            @RequestParam(value = "path")
                    String path,
            @NotNull
            @Valid
            @RequestBody
                    NewFilePath newFilePath) {
        system.lockDataNodes();
        system.moveFile(path, newFilePath.getNewPath());
        return ResponseEntity.ok().build();
    }

    @Log(LogLevel.INFO)
    @GetMapping(value = "/filesystem/files")
    public ResponseEntity<Resource> readFile(
            @NotNull
            @Valid
            @RequestParam(value = "path")
                    String path) {
        return ResponseEntity.ok(system.readFile(path));
    }

    @Log(LogLevel.INFO)
    @PostMapping(value = "/filesystem/files", consumes = {"multipart/form-data"})
    public ResponseEntity<Void> writeFile(
            @NotNull
            @Valid
            @RequestParam(value = "path")
                    String path,
            @NotNull
            @Valid
            @RequestParam(value = "file")
                    MultipartFile file) {
        system.lockDataNodes();
        system.writeFile(path, file);
        return ResponseEntity.ok().build();
    }

    @Log(LogLevel.INFO)
    @PostMapping(value = "/filesystem/files/create", consumes = {"application/json"})
    public ResponseEntity<Void> createFile(
            @NotNull
            @Valid
            @RequestBody
                    FilePath filePath) {
        system.lockDataNodes();
        system.writeFile(filePath.getPath(), null);
        return ResponseEntity.ok().build();
    }

    @Log(LogLevel.INFO)
    @PostMapping(value = "/filesystem/dirs", consumes = {"application/json"})
    public ResponseEntity<Void> createDirectory(
            @NotNull
            @Valid
            @RequestBody
                    FilePath filePath) {
        system.lockDataNodes();
        system.createDirectory(filePath.getPath());
        return ResponseEntity.ok().build();
    }

    @Log(LogLevel.INFO)
    @DeleteMapping(value = "/filesystem/dirs")
    public ResponseEntity<Void> deleteDirectory(
            @NotNull
            @Valid
            @RequestParam(value = "path")
                    String path) {
        system.lockDataNodes();
        system.deleteDirectory(path);
        return ResponseEntity.ok().build();
    }

    @Log(LogLevel.INFO)
    @GetMapping(value = "/filesystem/dirs", produces = {"application/json"})
    public ResponseEntity<DirectoryContent> readDirectory(
            @NotNull
            @Valid
            @RequestParam(value = "path")
                    String path) {
        return ResponseEntity.ok(system.readDirectory(path));
    }

    @Log(LogLevel.INFO)
    @GetMapping(value = "/filesystem/dirs/all", produces = {"application/json"})
    public ResponseEntity<Directories> readDirectories() {
        return ResponseEntity.ok(system.readDirectories());
    }
}
