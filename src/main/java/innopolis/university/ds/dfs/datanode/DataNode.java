package innopolis.university.ds.dfs.datanode;

import lombok.Getter;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class DataNode {

    private static final String HTTP = "http://";

    private final String host;
    private DatanodeState state = DatanodeState.NOT_INITIALIZED;
    private Set<String> dirs = new HashSet<>();
    private Set<String> files = new HashSet<>();
    private int offlineCount = 0;

    DataNode(String host) {
        this.host = host.startsWith(HTTP) ? host : HTTP + host;
    }

    void makeBusy() {
        state = DatanodeState.BUSY;
    }

    boolean isPingAvailable() {
        return state == DatanodeState.READY || state == DatanodeState.OFFLINE || isNotInitialized();
    }

    boolean isNotInitialized() {
        return state == DatanodeState.NOT_INITIALIZED;
    }

    boolean isAvailable() {
        return state == DatanodeState.READY || state == DatanodeState.BUSY;
    }

    void incrementOfflineCount() {
        offlineCount++;
    }

    void resetOfflineCount() {
        offlineCount = 0;
    }
}
