package innopolis.university.ds.dfs.datanode.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class FileInfo {

    private String name;

    private String description;

    private long size;

    private LocalDateTime lastModification;

    @JsonProperty("last_modification")
    public void setLastModification(String lastModification) {
        this.lastModification = LocalDateTime.parse(lastModification, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    public innopolis.university.ds.dfs.rest.dto.FileInfo toDto() {
        innopolis.university.ds.dfs.rest.dto.FileInfo dto = new innopolis.university.ds.dfs.rest.dto.FileInfo();
        dto.setName(name);
        dto.setDescription(description);
        dto.setSize(size);
        dto.setCreationDate(lastModification);
        return dto;
    }
}
