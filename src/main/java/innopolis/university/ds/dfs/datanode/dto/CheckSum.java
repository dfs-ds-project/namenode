package innopolis.university.ds.dfs.datanode.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CheckSum {

    private String checkSum;

    @JsonProperty("file_check_sum")
    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }
}
