package innopolis.university.ds.dfs.datanode;

import innopolis.university.ds.dfs.datanode.dto.*;
import innopolis.university.ds.dfs.exception.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static innopolis.university.ds.dfs.datanode.Endpoints.*;

@Log4j2
@Service
@RequiredArgsConstructor
public class DataNodeServiceImpl implements DataNodeService {

    @Value("${ping.maximumOffline}")
    private int offlineMaximum;

    private static final String HOST_OFFLINE = "Host %s is offline, ex = %s";
    private static final String HOST_REMOVED = "Host %s is removed from datanodes list.";

    private List<DataNode> dataNodes = new ArrayList<>();

    private ExecutorService executorService = Executors.newWorkStealingPool();

    private long availableSize = Long.MAX_VALUE;

    private synchronized long getAvailableSize() {
        return availableSize;
    }

    private synchronized void setAvailableSize(long size) {
        availableSize = size;
    }

    private boolean checkInfo(Set<String> dirs, Map<String, String> files, Set<String> hostDirs, Map<String, String> hostFiles) {
        return dirs.equals(hostDirs) && files.equals(hostFiles);
    }

    private void ping(DataNode host, Set<String> dirs, Map<String, String> files) {
        RestTemplate restTemplate = new RestTemplateBuilder()
                .setConnectTimeout(Duration.ofSeconds(2))
                .setReadTimeout(Duration.ofSeconds(2))
                .build();
        try {
            DatanodeInfo info = Optional.ofNullable(
                    restTemplate.getForObject(URI.create(host.getHost() + PING), DatanodeInfo.class)
            ).orElseThrow(RuntimeException::new);
            if (checkInfo(dirs, files, info.getDirs(), info.getFiles())) {
                host.setState(DatanodeState.READY);
                host.resetOfflineCount();
                log.info(String.format("Host %s is ready.", host.getHost()));
            } else {
                host.setState(DatanodeState.UPDATING);
                log.info(String.format("Host %s is updating.", host.getHost()));
                if (update(host, dirs, files)) {
                    host.setState(DatanodeState.READY);
                    host.resetOfflineCount();
                    log.info(String.format("Host %s finished update.", host.getHost()));
                }
            }
        } catch (ResourceAccessException ex) {
            host.setState(DatanodeState.OFFLINE);
            host.incrementOfflineCount();
            log.info(String.format(HOST_OFFLINE, host.getHost(), ex.getMessage()));
            if (host.getOfflineCount() > offlineMaximum) {
                dataNodes.remove(host);
                log.info(String.format(HOST_REMOVED, host.getHost()));
            }
        }
    }

    private boolean update(DataNode host, Set<String> dirs, Map<String, String> files) {
        Map<String, UpdateDatanode.FileUpdate> updateFiles = new HashMap<>();
        files.forEach((fileName, checkSum) -> {
            DataNode readyHostWithFile = dataNodes.parallelStream()
                    .filter(DataNode::isAvailable)
                    .filter(dataNode -> dataNode.getFiles().contains(fileName))
                    .findFirst()
                    .orElse(new DataNode("No host found."));
            updateFiles.put(fileName, new UpdateDatanode.FileUpdate(readyHostWithFile.getHost(), checkSum));
        });

        UpdateDatanode updateDatanode = new UpdateDatanode(dirs, updateFiles);
        HttpEntity<UpdateDatanode> requestEntity = new HttpEntity<>(updateDatanode);
        return postMethod(host, requestEntity, UPDATE);
    }

    private long initialize(DataNode host) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            AvailableSize hostAvailableSize = Optional.ofNullable(
                    restTemplate.getForObject(URI.create(host.getHost() + INIT), AvailableSize.class)
            ).orElseThrow(RuntimeException::new);
            host.setState(DatanodeState.READY);
            return hostAvailableSize.getAvailableSize();
        } catch (ResourceAccessException ex) {
            log.info(String.format(HOST_OFFLINE, host.getHost(), ex.getMessage()));
            host.setState(DatanodeState.OFFLINE);
            return Long.MAX_VALUE;
        }
    }

    private String writeFile(DataNode host, String path, byte[] bytes) {
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        if (bytes.length != 0) {
            MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
            fileMap.add(HttpHeaders.CONTENT_DISPOSITION,
                    ContentDisposition
                            .builder("form-data")
                            .name("file")
                            .filename(path)
                            .build().toString());

            body.add("file", new HttpEntity<>(bytes, fileMap));
        }

        body.add("path", path);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        RestTemplate restTemplate = new RestTemplate();
        try {
            return Optional.ofNullable(
                    restTemplate.postForObject(URI.create(host.getHost() + FILES), new HttpEntity<>(body, headers), CheckSum.class)
            ).orElseThrow(RuntimeException::new).getCheckSum();
        } catch (ResourceAccessException ex) {
            log.info(String.format(HOST_OFFLINE, host.getHost(), ex.getMessage()));
            host.setState(DatanodeState.OFFLINE);
            return null;
        }
    }

    private boolean copyFile(DataNode host, String path, String newPath) {
        return methodForFiles(host, HttpMethod.PUT, path, newPath);
    }

    private boolean moveFile(DataNode host, String path, String newPath) {
        return methodForFiles(host, HttpMethod.PATCH, path, newPath);
    }

    private boolean methodForFiles(DataNode host, HttpMethod method, String path, String newPath) {
        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("new_path", newPath);
        try {
            return restTemplate.exchange(
                    UriComponentsBuilder.fromUriString(host.getHost() + FILES).queryParam("path", path).build().toUri(),
                    method,
                    new HttpEntity<>(body),
                    String.class
            ).getStatusCode().is2xxSuccessful();
        } catch (ResourceAccessException ex) {
            log.info(String.format(HOST_OFFLINE, host.getHost(), ex.getMessage()));
            host.setState(DatanodeState.OFFLINE);
            return false;
        }
    }

    private Optional<FileInfo> getFileInfo(DataNode host, String path) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            return Optional.ofNullable(restTemplate.getForObject(
                    UriComponentsBuilder.fromUriString(host.getHost() + FILES_INFO).queryParam("path", path).build().toUri(),
                    FileInfo.class
            ));
        } catch (ResourceAccessException ex) {
            log.info(String.format(HOST_OFFLINE, host.getHost(), ex.getMessage()));
            host.setState(DatanodeState.OFFLINE);
            return Optional.empty();
        }
    }

    private Optional<Resource> readFile(DataNode host, String path) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            return Optional.ofNullable(restTemplate.getForObject(
                    UriComponentsBuilder.fromUriString(host.getHost() + FILES).queryParam("path", path).build().toUri(),
                    Resource.class
            ));
        } catch (ResourceAccessException ex) {
            log.info(String.format(HOST_OFFLINE, host.getHost(), ex.getMessage()));
            host.setState(DatanodeState.OFFLINE);
            return Optional.empty();
        }
    }

    private boolean createDirectory(DataNode host, String path) {
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("path", path);
        return postMethod(host, new HttpEntity<>(body), DIRS);
    }


    private boolean deleteFile(DataNode host, String path) {
        return deleteMethod(host, path, FILES);
    }

    private boolean deleteDirectory(DataNode host, String path) {
        return deleteMethod(host, path, DIRS);
    }

    private boolean deleteMethod(DataNode host, String path, String url) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate.exchange(
                    UriComponentsBuilder.fromUriString(host.getHost() + url).queryParam("path", path).build().toUri(),
                    HttpMethod.DELETE,
                    null,
                    String.class
            ).getStatusCode().is2xxSuccessful();
        } catch (ResourceAccessException ex) {
            log.info(String.format(HOST_OFFLINE, host.getHost(), ex.getMessage()));
            host.setState(DatanodeState.OFFLINE);
            return false;
        }
    }

    private boolean postMethod(DataNode host, HttpEntity entity, String url) {
        RestTemplate restTemplate = new RestTemplate();
        try {
            return restTemplate.postForEntity(URI.create(host.getHost() + url), entity, String.class)
                    .getStatusCode().is2xxSuccessful();
        } catch (ResourceAccessException ex) {
            log.info(String.format(HOST_OFFLINE, host.getHost(), ex.getMessage()));
            host.setState(DatanodeState.OFFLINE);
            return false;
        }
    }

    @Override
    public void ping(Set<String> dirs, Map<String, String> files) {
        dataNodes.parallelStream()
                .filter(DataNode::isPingAvailable)
                .forEach(host -> executorService.execute(() -> ping(host, dirs, files)));
        dataNodes.parallelStream()
                .filter(DataNode::isNotInitialized)
                .forEach(this::initialize);
    }

    @Override
    public long initialize() {
        setAvailableSize(Long.MAX_VALUE);
        dataNodes.parallelStream()
                .forEach(host -> {
                    long hostSize = initialize(host);
                    if (getAvailableSize() > hostSize) {
                        setAvailableSize(hostSize);
                    }
                });

        return getAvailableSize();
    }

    @Override
    public void writeFile(String path, byte[] bytes, String checkSum) {
        dataNodes.parallelStream()
                .filter(DataNode::isAvailable)
                .forEach(host -> executorService.execute(() -> {
                    String check = writeFile(host, path, bytes);
                    if (checkSum.equalsIgnoreCase(check)) {
                        host.getFiles().add(path);
                    }
                    host.setState(DatanodeState.READY);
                }));
    }

    @Override
    public void deleteFile(String path) {
        dataNodes.parallelStream()
                .filter(DataNode::isAvailable)
                .forEach(host -> executorService.execute(() -> {
                    if (deleteFile(host, path)) {
                        host.getFiles().remove(path);
                    }
                    host.setState(DatanodeState.READY);
                }));
    }

    @Override
    public void copyFile(String path, String newPath) {
        dataNodes.parallelStream()
                .filter(DataNode::isAvailable)
                .forEach(host -> executorService.execute(() -> {
                    if (copyFile(host, path, newPath)) {
                        host.getFiles().add(newPath);
                    }
                    host.setState(DatanodeState.READY);
                }));
    }

    @Override
    public void moveFile(String path, String newPath) {
        dataNodes.parallelStream()
                .filter(DataNode::isAvailable)
                .forEach(host -> executorService.execute(() -> {
                    if (moveFile(host, path, newPath)) {
                        host.getFiles().remove(path);
                        host.getFiles().add(newPath);
                    }
                    host.setState(DatanodeState.READY);
                }));
    }

    @Override
    public FileInfo getFileInfo(String path) {
        Collection<DataNode> availableDataNodes = dataNodes.parallelStream()
                .filter(DataNode::isAvailable)
                .filter(dataNode -> dataNode.getFiles().contains(path))
                .collect(Collectors.toList());

        Optional<FileInfo> info = Optional.empty();

        for (DataNode host : availableDataNodes) {
            info = getFileInfo(host, path);
            if (info.isPresent()) {
                break;
            }
        }

        return info.orElseThrow(() -> new NotFoundException("Sorry, no file found in the system."));
    }

    @Override
    public Resource readFile(String path) {
        Collection<DataNode> availableDataNodes = dataNodes.parallelStream()
                .filter(DataNode::isAvailable)
                .filter(dataNode -> dataNode.getFiles().contains(path))
                .collect(Collectors.toList());

        Optional<Resource> file = Optional.empty();

        for (DataNode host : availableDataNodes) {
            file = readFile(host, path);
            if (file.isPresent()) {
                break;
            }
        }

        return file.orElseThrow(() -> new NotFoundException("Sorry, no file found in the system."));
    }

    @Override
    public void createDirectory(String path) {
        dataNodes.parallelStream()
                .filter(DataNode::isAvailable)
                .forEach(host -> executorService.execute(() -> {
                    if (createDirectory(host, path)) {
                        host.getDirs().add(path);
                    }
                    host.setState(DatanodeState.READY);
                }));
    }

    @Override
    public void deleteDirectory(String path) {
        dataNodes.parallelStream()
                .filter(DataNode::isAvailable)
                .forEach(host -> executorService.execute(() -> {
                    if (deleteDirectory(host, path)) {
                        host.getDirs().remove(path);
                    }
                    host.setState(DatanodeState.READY);
                }));
    }

    @Override
    public void addDataNode(String host, boolean restored) {
        DataNode dataNode = new DataNode(host);
        if (restored) {
            dataNode.setState(DatanodeState.OFFLINE);
        }
        dataNodes.add(dataNode);
    }

    @Override
    public void lock() {
        dataNodes.parallelStream()
                .filter(DataNode::isAvailable)
                .forEach(DataNode::makeBusy);
    }
}
