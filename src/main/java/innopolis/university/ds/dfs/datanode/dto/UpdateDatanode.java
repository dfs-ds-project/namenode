package innopolis.university.ds.dfs.datanode.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;
import java.util.Map;

@Data
@AllArgsConstructor
public class UpdateDatanode {

    private Collection<String> dirs;

    private Map<String, FileUpdate> files;

    @Data
    @AllArgsConstructor
    public static class FileUpdate {

        private String host;

        @JsonProperty("file_check_sum")
        private String checkSum;
    }
}
