package innopolis.university.ds.dfs.datanode;

public class Endpoints {

    private Endpoints() {
    }

    public static final String PREFIX = "/api/";
    public static final String SYSTEM = PREFIX + "system/";
    public static final String PING = SYSTEM + "ping/";
    public static final String INIT = SYSTEM + "init/";
    public static final String UPDATE = SYSTEM + "update/";
    public static final String FILE_SYSTEM = PREFIX + "filesystem/";
    public static final String FILES = FILE_SYSTEM + "files/";
    public static final String FILES_INFO = FILES + "info/";
    public static final String DIRS = FILE_SYSTEM + "dirs/";
}
