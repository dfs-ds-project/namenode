package innopolis.university.ds.dfs.datanode.dto;

import lombok.Data;

import java.util.Map;
import java.util.Set;

@Data
public class DatanodeInfo {

    private Set<String> dirs;

    private Map<String, String> files;
}
