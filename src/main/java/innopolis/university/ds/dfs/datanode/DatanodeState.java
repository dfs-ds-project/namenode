package innopolis.university.ds.dfs.datanode;

public enum DatanodeState {

    READY,
    UPDATING,
    NOT_INITIALIZED,
    BUSY,
    OFFLINE
}
