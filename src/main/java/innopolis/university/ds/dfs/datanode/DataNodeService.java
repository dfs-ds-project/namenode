package innopolis.university.ds.dfs.datanode;

import innopolis.university.ds.dfs.datanode.dto.FileInfo;
import org.springframework.core.io.Resource;

import java.util.Map;
import java.util.Set;

public interface DataNodeService {

    void ping(Set<String> dirs, Map<String, String> files);

    long initialize();

    void writeFile(String path, byte[] bytes, String checkSum);

    void deleteFile(String path);

    void copyFile(String path, String newPath);

    void moveFile(String path, String newPath);

    FileInfo getFileInfo(String path);

    Resource readFile(String path);

    void createDirectory(String path);

    void deleteDirectory(String path);

    void addDataNode(String host, boolean restored);

    void lock();
}
