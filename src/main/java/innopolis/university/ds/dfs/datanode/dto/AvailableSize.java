package innopolis.university.ds.dfs.datanode.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AvailableSize {

    private long availableSize;

    @JsonProperty("available_size")
    public void setAvailableSize(long availableSize) {
        this.availableSize = availableSize;
    }
}
