package innopolis.university.ds.dfs.system;

import innopolis.university.ds.dfs.datanode.dto.FileInfo;
import innopolis.university.ds.dfs.rest.dto.Directories;
import innopolis.university.ds.dfs.rest.dto.DirectoryContent;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;
import java.util.Set;

public interface System {

    long initialize();

    void ping();

    void writeFile(String path, MultipartFile file);

    void deleteFile(String path);

    void copyFile(String path, String newPath);

    void moveFile(String path, String newPath);

    FileInfo getFileInfo(String path);

    Resource readFile(String path);

    DirectoryContent readDirectory(String path);

    Directories readDirectories();

    void createDirectory(String path);

    void deleteDirectory(String path);

    Set<String> getDirs();

    Map<String, String> getFiles();

    void addDataNode(String host, boolean restored);

    void lockDataNodes();
}
