package innopolis.university.ds.dfs.system;

import innopolis.university.ds.dfs.datanode.DataNodeService;
import innopolis.university.ds.dfs.datanode.dto.FileInfo;
import innopolis.university.ds.dfs.exception.BadRequestException;
import innopolis.university.ds.dfs.exception.ForbiddenException;
import innopolis.university.ds.dfs.rest.dto.Directories;
import innopolis.university.ds.dfs.rest.dto.DirectoryContent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.logging.LogLevel;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import ru.tinkoff.eclair.annotation.Log;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service
@RequiredArgsConstructor
public class SystemImpl implements System {

    private Set<String> dirs = new HashSet<>(ROOT_DIR);

    private Map<String, String> files = new HashMap<>();

    private final DataNodeService datanodeService;

    private static final String FILE_PATTERN = "^(\\w+/)*(\\w+.\\w+)$";
    private static final String DIR_PATTERN = "^((\\w+/)*\\w+)?$";
    private static final Collection<String> ROOT_DIR = Collections.singletonList("");

    @Override
    public synchronized Set<String> getDirs() {
        return dirs;
    }

    @Override
    public synchronized Map<String, String> getFiles() {
        return files;
    }

    private void validatePath(String path, String pattern) {
        if (!path.matches(pattern)) {
            throw new BadRequestException(String.format("The path %s is not valid.", path));
        }
    }

    private void validateFilePath(String path) {
        if (StringUtils.isEmpty(path)) {
            throw new BadRequestException("The path can not be empty.");
        }
        validatePath(path, FILE_PATTERN);
    }

    private void validateDirPath(String path) {
        validatePath(path, DIR_PATTERN);
    }

    private void validateFilePathToCreate(String path) {
        validateFilePath(path);
        int endIndex = path.lastIndexOf('/');
        if (endIndex > 0) {
            validateDirExist(path.substring(0, endIndex));
        }
    }

    private void validateFilePathToGet(String path) {
        validateFilePathToCreate(path);
        if (!files.containsKey(path)) {
            throw new ForbiddenException(String.format("The file %s does not exist.", path));
        }
    }

    private void validateDirExist(String path) {
        if (!dirs.contains(path)) {
            throw new ForbiddenException(String.format("The directory %s does not exist.", path));
        }
    }

    private void validateDirPathToCreate(String path) {
        validateDirPath(path);
        int endIndex = path.lastIndexOf('/');
        if (endIndex > 0) {
            validateDirExist(path.substring(0, endIndex));
        }
    }

    private void validateDirPathToGet(String path) {
        validateDirPathToCreate(path);
        validateDirExist(path);
    }

    @Override
    public long initialize() {
        dirs = new HashSet<>(ROOT_DIR);
        files.clear();
        return datanodeService.initialize();
    }

    @Override
    @Log(LogLevel.INFO)
    @Scheduled(fixedDelayString = "${ping.delayInMilliseconds}")
    public void ping() {
        Set<String> pingDirs = new HashSet<>(dirs);
        pingDirs.removeAll(ROOT_DIR);
        datanodeService.ping(pingDirs, files);
    }

    @Override
    public void writeFile(String path, MultipartFile file) {
        try {
            validateFilePathToCreate(path);
            byte[] bytes = file != null ? file.getBytes() : new byte[0];
            String checkSum = DigestUtils.md5DigestAsHex(bytes);
            files.put(path, checkSum);
            datanodeService.writeFile(path, bytes, checkSum);
        } catch (IOException e) {
            log.error("Cannot read the file.", e);
        }
    }

    @Override
    public void deleteFile(String path) {
        validateFilePathToGet(path);
        files.remove(path);
        datanodeService.deleteFile(path);
    }

    @Override
    public void copyFile(String path, String newPath) {
        validateFilePathToGet(path);
        files.put(newPath, files.get(path));
        datanodeService.copyFile(path, newPath);
    }

    @Override
    public void moveFile(String path, String newPath) {
        validateFilePathToGet(path);
        files.put(newPath, files.remove(path));
        datanodeService.moveFile(path, newPath);
    }

    @Override
    public FileInfo getFileInfo(String path) {
        validateFilePathToGet(path);
        return datanodeService.getFileInfo(path);
    }

    @Override
    public Resource readFile(String path) {
        validateFilePathToGet(path);
        return datanodeService.readFile(path);
    }

    @Override
    public DirectoryContent readDirectory(String path) {
        validateDirPathToGet(path);
        Set<String> readDirs = new HashSet<>(dirs);
        readDirs.removeAll(ROOT_DIR);
        DirectoryContent result = new DirectoryContent();
        result.setFiles(files.keySet().parallelStream()
                .filter(file -> file.startsWith(path) && file.indexOf('/', path.length() + 1) < 0)
                .collect(Collectors.toList()));
        result.setDirs(readDirs.parallelStream()
                .filter(file -> !file.equals(path) && file.startsWith(path) && file.indexOf('/', path.length() + 1) < 0)
                .collect(Collectors.toList()));
        return result;
    }

    @Override
    public Directories readDirectories() {
        return new Directories(dirs);
    }

    @Override
    public void createDirectory(String path) {
        validateDirPathToCreate(path);
        dirs.add(path);
        datanodeService.createDirectory(path);
    }

    @Override
    public void deleteDirectory(String path) {
        validateDirPathToGet(path);
        dirs.remove(path);
        files.keySet().removeIf(filePath -> filePath.startsWith(path));
        datanodeService.deleteDirectory(path);
    }

    @Override
    public void addDataNode(String host, boolean restored) {
        datanodeService.addDataNode(host, restored);
    }

    @Override
    public void lockDataNodes() {
        datanodeService.lock();
    }
}
